<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>

	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="assets/css/main.css" rel="stylesheet"/>
</head>

<body class="text-center">
	<form class="form-signin" id="login-form"></form>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			let accessToken = "<?php echo $_COOKIE['ACCESSTOKEN']; ?>";

			if (accessToken != '') {
				$('#login-form').html(
					`<h3>Welcome!</h3>
					<p>You are logged in as
						<br><br>
						<img id="image" class="rounded mx-auto d-block">
						<br>
						<strong id="name"></strong>
					</p>
					<a href="https://ccfdm2.dev2.creatinginfo.com/logout">Logout</a>
					`
				);

				fetch('https://ccfdm2.dev2.creatinginfo.com/api/v2/me', {
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						"Authorization": "Bearer " + accessToken
					}
				})
				.then(res => res.json()) // parse response as JSON (can be res.text() for plain response)
				.then(response => {
					$("#name").text(`${ response.firstName } ${ response.lastName }`);
					$("#image").attr('src', response.image);
				})
				.catch(err => {
					alert("sorry, there are no results for your search")
				});
			} else {
				$('#login-form').html(
					`<h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
					<input type="text" name="username" class="form-control" placeholder="Username" required autofocus>
					<input type="password" name="password" class="form-control" placeholder="Password" required>
					<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>`
				);
			}

			$('#login-form').submit(function(e) {
				e.preventDefault();

				let username = $('input[name="username"]').val();
				let password = $('input[name="password"]').val();

				fetch('https://ccfdm2.dev2.creatinginfo.com/oauth/v2/token', {
					method: 'POST',
					headers: {
						"Content-Type": "application/x-www-form-urlencoded",
					},
					body: `username=${ username }&password=${ password }&grant_type=password`
				})
				.then(res => res.json()) // parse response as JSON (can be res.text() for plain response)
				.then(response => {
					if (response.access_token != null) {
						document.cookie = 'ACCESSTOKEN=' + response.access_token + ';path=/;max-age=3600;domain=.dev2.creatinginfo.com;secure';

						window.location = 'index.php';
					}

					if (response.error != null) {
						alert(response.error_description);
					}
				});
			})
		})
	</script>
</body>
</html>
